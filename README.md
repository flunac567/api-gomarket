# GoMarket API REST

Esta es la API utilizada para el desarrollo de la aplicación GoMarket,la cual permite autenticar usuarios,designarle roles,realizar procesos CRUD y dar permisos a los usuarios para realiar acciones según su rol.

## Requisitos

Antes de utilizar el proyecto de este repositorio asegurese de tener instaladas las siguientes herramientas:

* [NodeJs](https://nodejs.org/en/) - Interprete de Javascript usado para la instalacion de dependencias y ejecución del servidor.
* [MongoDB](https://www.mongodb.com) - Base de Datos utilizada
* [Babel](https://babeljs.io) - Usado para convertir el código de Ema Script 6 (ES6) a Javascript entendible para cualqueir navegador.

Si no cuenta con estas herramientas ,también puede utilizar el proyecto en un contenedor de [Docker](https://www.docker.com) con el siguiente comando:

```
docker run -d -p miPuerto:4000 api-gomarket
```
Antes de ejecutar el comando, cambie "miPuerto" por el puerto de su preferencia.
## Uso

Para poder hacer uso del código de este proyecto deberá poseer un Editor de código o un IDE que soporte [Babel](https://babeljs.io) y [NodeJs](https://nodejs.org/en/).Asegurandose que cuente con ello, deberá descargar este proyecto desde el botón "DownLoad" o si cuenta con [Git](https://git-scm.com), puede hacer un Git Clone de este repositorio.

```
git clone https://gitlab.com/flunac567/api-gomarket.git
```

## Instalación

Ya sea que lo instalo con Docker o con todas las herramientas mencionadas en los requisitos, deberá utilizar el siguiente comando para instalar las dependencias del proyecto.

```
npm install
```
## Configuración de variables de entorno

Para que el proyecto funcione correctamente será necesario que cree variables de entorno en un archivo .ENV afuera de la carpeta "src", este archivo debera contener las siguientes variables:

```
* MONGODB_PROD - Cadena de conexión a la base de datos
* SECRET - Clave secreta para el token
```
## Ejecución

El proyecto cuenta con 2 modos de ejecución:

### Modo de desarrollo

Para ejecutar la aplicación en un entorno de desarrollo ejecute este comando:

```
npm run dev
```
### Modo de producción

Para ejecutar la aplicación en un entorno de producción seran necesarios unos pasos extra.Primero realice un "build" del proyecto para convertir el codigo de ES6 a codigo javascript puro. Utilice: 

```
npm run build
```

Luego de ello proceda a eejcutar el proyecto con :

```
npm start
```
En modo de producción las variables de entorno no deberian funcionar ,por lo que será normal si el servidor no logra conectarse a la base de datos.

Para que el API funcione correctamente asegurese de realizar la configuraciones correspondientes en su servidor de producción.


