import express from "express";
import morgan from "morgan";
import cors from "cors";
import pkg from "../package.json";

//Setup
import { crearCategorias, crearPresidente, crearRoles } from "./lib/setup";
//Rutas importadas
import categoriaRoutes from "./routes/categoria.routes";
import autenticacionRoutes from "./routes/autenticacion.routes";
import productoRoutes from "./routes/producto.routes";
import usuarioRoutes from "./routes/usuario.routes";
import pedidoRoutes from "./routes/pedido.routes";
import reservaRoutes from "./routes/reservas.routes";
import puestoRoutes from "./routes/puesto.routes";
//Manejo de errores
import { notFound,errorHadler } from "./middlewares/errores";

const app=express();
//setup
crearRoles();
crearCategorias();
crearPresidente();
// Configuracion
app.set("pkg", pkg);
app.set("port", process.env.PORT || 4000);
app.set("json spaces", 4);

app.use(morgan('dev'));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use('/uploads', express.static('uploads'));

//Configurando passport
/*
app.use(passport.initialize());
passport.use(passportMiddleware); 
*/


// Ruta de bienvenida
app.get("/", (req, res) => {
  res.json({
    message: "Bienvenido a la API de GoMarket",
    name: app.get("pkg").name,
    version: app.get("pkg").version,
    description: app.get("pkg").description,
    author: app.get("pkg").author,
  });
});

//Rutas 
app.use('/api/autenticacion',autenticacionRoutes);
app.use('/api/categorias',categoriaRoutes);
app.use('/api/productos',productoRoutes);
app.use('/api/usuarios',usuarioRoutes);
app.use('/api/pedidos',pedidoRoutes);
app.use('/api/reservas',reservaRoutes);
app.use('/api/puesto',puestoRoutes);
//Errores
app.use(notFound);
app.use(errorHadler);

export default app;