
if (process.env.NODE_ENV != "production") {
  require("dotenv").config();
}

export default {
  MONGODB_URI: process.env.MONGODB_PROD, 
  PORT: process.env.PORT || 4000,
  SECRET: process.env.SECRET,
  CLOUDINARY_CLOUD_NAME: process.env.CLOUDINARY_CLOUD_NAME,
  CLOUDINARY_API_KEY:process.env.CLOUDINARY_API_KEY,
  CLOUDINARY_API_SECRET: process.env.CLOUDINARY_API_SECRET
};
