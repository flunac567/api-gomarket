import Usuario from "../models/usuario";
import jwt from "jsonwebtoken";
import config from "../config";
import Rol from "../models/rol";
import Categoria from "../models/categoria";
import cloudinary from "./../cloudinary_config";
import fs from 'fs-extra';

//CREAR TOKEN
function crearToken(usuario) {
    const rol= Rol.findOne({_id: {$in: usuario.rolId}})
    const tipoDeRol=rol.tipoDeRol
    return jwt.sign({ 
        id: usuario._id, 
        nombre: usuario.nombreUsuario, 
        apellido: usuario.apellidoUsuario, 
        edad:usuario.edad,
        genero:usuario.genero,
        correo: usuario.correo,
        rolId:tipoDeRol
      }, config.SECRET, {
        expiresIn: 86400, // 24 hours
      });
}

//REGISTRO
export const registro = async(req,res)=>{
    //Guardando en cloudinary
    const result= await cloudinary.v2.uploader.upload(req.file.path);

    const { nombreUsuario,apellidoUsuario,edad,genero,correo,contraseña,rolId} =req.body;
    
    const nuevoUsuario =new Usuario({
        imgPerfil:result.secure_url,
        nombreUsuario,
        apellidoUsuario,
        edad,
        genero,
        correo,
        contraseña: await Usuario.encryptPassword(contraseña),
        imgPerfilId:result.public_id,
    });
    //Validar rol
    if(req.body.rolId){
        //identificar su rol
        const buscarRol=await Rol.find({tipoDeRol: { $in: rolId } });
        nuevoUsuario.rolId= buscarRol.map((rol)=> rol._id);
    }else {
        //Darle el rol 'Cliente' por default
        const rolPorDefecto=await Rol.findOne({tipoDeRol: "Cliente"});
        nuevoUsuario.rolId=[rolPorDefecto._id];
    }
    //Agregar Categoria
    
    
    /*
    if(req.body.categoriaId !=null ){
       //Validar Categoria
       if(req.body.categoriaId){
           const buscarCategoria=await Categoria.find({nombreCategoria: { $in: categoriaId } });
           nuevoUsuario.categoriaId = buscarCategoria.map((categoria)=>categoria._id);
        }else{
            //Darle la categoria abarrotes por default
           const categoria=await Categoria.findOne({nombreCategoria: "Abarrotes" });
           nuevoUsuario.categoriaId = [categoria._id];
        } 
    }else {
        req.body.categoriaId= null;
    }
    */
    //Guardar usuario en la DB
    const usuarioAgregado = await nuevoUsuario.save();
    //Eliminamos la imagen de la carpeta local
    await fs.unlink(req.file.path);
    //Respuesta del servidor
    res.status(200).json({ token:crearToken(usuarioAgregado) });
}

//LOGIN
export const login=async(req,res)=>{
    try {
         if (!req.body.correo || !req.body.contraseña) {
             return res.status(400).send({'mensaje':'Coloque un usuario y una contraseña'})
         }
        //Verificar correo
        const buscarUsuario= await Usuario.findOne({ correo: req.body.correo }).populate(
            "rolId"
       );
   
       if(!buscarUsuario) return res.status(400).json({ mensaje: "Usuario no encontrado"});
       
       //Verificar contraseña
       const compararPassword=await Usuario.comparePassword(
           req.body.contraseña,
           buscarUsuario.contraseña
       );
       
       if (!compararPassword) {
           return res.status(401).json({
               token:null,
               message:" Contraseña no valida"
           });
       }
       
       //Generar Token
       const rol= await Rol.findOne({_id: {$in: buscarUsuario.rolId}})
       const tipoDeRol=rol.tipoDeRol
       const token = jwt.sign({ 
           id: buscarUsuario._id, 
           nombre: buscarUsuario.nombreUsuario, 
           apellido: buscarUsuario.apellidoUsuario, 
           edad:buscarUsuario.edad,
           genero:buscarUsuario.genero,
           correo: buscarUsuario.correo,
           rolId:tipoDeRol
         }, config.SECRET, {
           expiresIn: 86400, // 24 hours
         });
        
         res.json({ token});
    } catch (error) {
        console.log(error);
    }
}

