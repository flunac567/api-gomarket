import Usuario from "../models/usuario";
import jwt from "jsonwebtoken";
import config from "../config";
import Rol from "../models/rol";
import Categoria from "../models/categoria";

//REGISTRO
export const registro = async(req,res)=>{
    
    const { nombreUsuario,apellidoUsuario,correo,contraseña,rolId,categoriaId } =req.body;
    
    const nuevoUsuario =new Usuario({
        nombreUsuario,
        apellidoUsuario,
        correo,
        contraseña: await Usuario.encryptPassword(contraseña)
    });
    //Validar rol
    if(req.body.rolId){
        //identificar su rol
        const buscarRol=await Rol.find({tipoDeRol: { $in: rolId } });
        nuevoUsuario.rolId= buscarRol.map((rol)=> rol._id);
    }else {
        //Darle el rol 'Cliente' por default
        const rolPorDefecto=await Rol.findOne({tipoDeRol: "Cliente"});
        nuevoUsuario.rolId=[rolPorDefecto._id];
    }
    //Agregar Categoria
    if(req.body.categoriaId !=null ){
       //Validar Categoria
       if(req.body.categoriaId){
           const buscarCategoria=await Categoria.find({nombreCategoria: { $in: categoriaId } });
           nuevoUsuario.categoriaId = buscarCategoria.map((categoria)=>categoria._id);
        }else{
            //Darle la categoria abarrotes por default
           const categoria=await Categoria.findOne({nombreCategoria: "Abarrotes" });
           nuevoUsuario.categoriaId = [categoria._id];
        } 
    }else {
        req.body.categoriaId= null;
    }
    
    //Guardar usuario en la DB
    const usuarioAgregado = await nuevoUsuario.save();
    //Crear su token
    const token = jwt.sign({id: usuarioAgregado._id },config.SECRET,{
         expiresIn:86400,
    });

    res.status(200).json({ token });
}

//LOGIN
export const login=async(req,res)=>{
    try {
         if (!req.body.correo || !req.body.contraseña) {
             return res.status(400).send({'mensaje':'Coloque un usuario y una contraseña'})
         }
        //Verificar correo
        const buscarUsuario= await Usuario.findOne({ correo: req.body.correo }).populate(
            "rolId"
       );
   
       if(!buscarUsuario) return res.status(400).json({ message: "Usuario no encontrado"});
       
       //Verificar contraseña
       const compararPassword=await Usuario.comparePassword(
           req.body.contraseña,
           buscarUsuario.contraseña
       );
       
       if (!compararPassword) {
           return res.status(401).json({
               token:null,
               message:" Contraseña no valida"
           });
       }
       
       //Generar Token
       crearToken(buscarUsuario);
     
    } catch (error) {
        console.log(error);
    }
}

//OBTENER DATOS
export const datosUsuario=async(req,res)=>{
    
    const usuario= await Usuario.findOne({correo: req.body.correo})
    const rol= await Rol.findOne({_id: {$in: usuario.rolId}})
    const tipoDeRol=rol.tipoDeRol
    
    //console.log(tipoDeRol)
    const info={
        usuario,
        tipoDeRol
    }



    res.status(200).json(info);
}