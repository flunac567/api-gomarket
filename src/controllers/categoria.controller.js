import cloudinary from "./../cloudinary_config";
import Categoria from "../models/categoria";
import fs from 'fs-extra';
import path from 'path'

//CREAR CATEGORIAS
export const crearCategoria=async (req,res,next)=>{

    try {
        //Guardando en cloudinary
        const result= await cloudinary.v2.uploader.upload(req.file.path);
        
        console.log(result)
        //Guardando en la base de datos
        const {nombreCategoria}=req.body;
        const nuevaCategoria=new Categoria({nombreCategoria,imgCategoria:result.secure_url,publicId: result.public_id});
        const categoriaAgregada=await nuevaCategoria.save(); 
        //Eliminamos la imagen de la carpeta local
        await fs.unlink(req.file.path);
        //Respuesta del servidor
        res.status(201).json({mensaje: 'Nueva categoria agregada',categoriaAgregada});
    } catch (error) {
        next(error)
    }
    
}

//LISTAR CATEGORIAS
export const obtenerCategorias=async (req,res,next)=>{

        const categorias=await Categoria.find({},{createdAt:0,updatedAt:0});
        res.json(categorias);
    
}

//LISTAR CATEGORIAS
export const clienteCategorias=async (req,res )=>{

        try {
            const categorias= await Categoria.find({},{createdAt:0,updatedAt:0});
            res.json(categorias)
        } catch (error) {
            next(error)
        }
   
}
//BUSCAR CATEGORIAS
export const buscarCategoria=async (req,res)=>{
    const categoria= await Categoria.findById(req.params.categoriaId)
    res.status(200).json(categoria);
}

//ACTUALIZAR CATEGORIAS
export const actualizarCategoria=async (req,res)=>{
    const categoriaActualizada= await Categoria.findByIdAndUpdate(req.params.categoriaId, req.body, {
        new: true
    })
    res.status(200).json(categoriaActualizada)
}

//Eliminar una Categoria
export const eliminarCategoria= async(req,res)=>{
    const {categoriaId}=req.params;
    await Categoria.findByIdAndDelete(categoriaId);
    res.status(204).json();
}

//ELIMINAR CATEGORIAS
export const eliminarCategoriaCloud=async(req,res,next)=>{
    try {
        const {categoriaId}=req.params;
        const categoriaEliminada =await Categoria.findByIdAndDelete(categoriaId);
        const result= await cloudinary.v2.uploader.destroy(categoriaEliminada.publicId);
        
        console.log(categoriaEliminada.publicId)
        console.log(result)
        res.status(201).json({mensaje:'Categoria eliminada'})
    } catch (error) {
        next(error)
    }
}

