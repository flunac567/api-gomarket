import cloudinary from "../cloudinary_config";
import fs from 'fs-extra';
import qrcode from "qrcode";
import Pedido from "../models/pedido";
import Rol from "../models/rol";
import Usuario from "../models/usuario";
import Puesto from "../models/puestos";
import Producto from "../models/producto";

//CREAR PEDIDO
export const crearPedido= async(req,res)=> {
    //Guardar Reserva
    const vendedor= await Puesto.findOne({_id:req.params.vendedorId},{_id:1})
    const vendedores=vendedor._id

    const { estado,clienteId} = req.body;
    
    const obtenerDetallePedido= 'true'
    //codigoQR
    const QR=await qrcode.toDataURL(obtenerDetallePedido)
    //Guardando en cloudinary
    const result= await cloudinary.v2.uploader.upload(QR);
    console.log(result)
    //Configurar fecha de entrega
    const fecha= new Date()
    const manana=fecha.setDate(fecha.getDate()+1);
    //Creando la reserva
    const nuevoPedido= new Pedido({
        fechaDeEntrega: manana,
        estado,
        codigoQR:result.secure_url,
        codigoQR_Id:result.public_id,
        vendedorId:vendedores
    });
    //identificar su rol
    const buscarCliente=await Usuario.find({correo: { $in: clienteId } });
    nuevoPedido.clienteId= buscarCliente.map((usuario)=> usuario._id)
    //Guardarla en MONGODB
    const pedidoAgregado=await nuevoPedido.save();
   
    console.log(pedidoAgregado)

    //Respuestas del servidor
    res.status(201).json({mensaje:'Pedido Guardado',pedidoAgregado})
    
}

export const reservarProductos=async(req,res,next)=>{
    //Obtener el Id del productp
    const productoId=req.params.productoId
    const producto=await Producto.find({_id:productoId});
    const productos=producto._id
    //recoger la cantidad
    const {cantidad,pedidoId}=req.body
    //asignar el precio dependiendo de la cantidad
    const precioProd= producto.map(p=>{ p.precio *= cantidad; return p.precio } )


    const reservaId=await Pedido.findOne({ codigoQR_Id: { $in:pedidoId } })
    const codigoId=reservaId._id
    

    try {
        const reservarProducto=await Pedido.findByIdAndUpdate(codigoId,
              {
                  reservas:{
                      productoId:productoId,
                      cantidad:cantidad,
                      precioTotal:precioProd
                  }
              }
            )

        console.log(productoId,cantidad,precioProd,codigoId)
        res.status(201).json({mensaje:'Producto agregado a la reserva',reservarProducto});
    } catch (error) {
        next(error)
    }
}


export const reservaProd=async(req,res,next)=>{
    //Obtener el Id del productp
    const productoId=req.params.productoId
    //recoger la cantidad
    const {cantidad,pedidoId}=req.body
    //asignar el precio dependiendo de la cantidad
    const precioProd= producto.map(p=>{ p.precio *= cantidad; return p.precio } )
}

//LISTAR DE PEDIDOS
export const listarPedidos=async(req,res)=>{
    const pedido=await Pedido.find({},{estado:1,fechaDeEntrega:1,codigoQR:1});
    res.json(pedido);  
}

//prueba
export const pruebaReservas=async(req,res)=>{
 
    const {clienteId}= req.body
    //identificar su rol
    const buscarCliente=await Usuario.find({correo: { $in: clienteId } });
    const reserva= buscarCliente.map((usuario)=> usuario._id);
    console.log(reserva)
 res.json(reserva)
    
}

//BUSCAR RESERVA
export const buscarPedido=async (req,res)=>{
    const pedido=await Pedido.aggregate(
        [{
                $match: {
                    _id: req.params.pedidoId
                }                  
        }]
    )

    res.status(200).json(pedido);
}

//ACTUALIZAR RESERVAS
export const actualizarPedido=async (req,res)=>{
    const pedidoAtualizada=await Pedido.findByIdAndUpdate(req.params.pedidoId,req.body,{
        new:true
    });
   res.status(200).json(pedidoAtualizada);
}

//ELIMINAR RESERVAS
/*
export const eliminarReserva=async (req,res)=>{
    const { reservaId }=req.params;
    await Reserva.findByIdAndDelete(reservaId);
    res.status(204).json();
}
*/

//ELIMINAR RESERVAS
export const eliminarPedidoCloud=async (req,res)=>{
    const { pedidoId }=req.params;
    const pedidoCloud=await Pedido.findByIdAndDelete(pedidoId);
    const result= await cloudinary.v2.uploader.destroy(pedidoCloud.codigoQR_Id);
    console.log(result)
    res.status(204).json({mensaje:"Pedido eliminado con exito"});
}

//PRECIO TOTAL DE LA RESERVA
export const precioTotal=async (req,res)=>{
    const { reservaId }=req.params;
    await Reserva.findOne(reservaId);
}

//CLIENTES  UE HAN HECHO RESERVAS
export const clientePedido=async(req,res)=>{
    const reserva= await Pedido.find({})
    const clientes= reserva.clienteId
    const prueba=await Usuario.where('_id').equals(clientes)
console.log(clientes)
        res.status(200).json(prueba);
}

//MIS PEDIDOS COMO CLIENTE
export const listaClientesPedidos=async(req,res)=>{

    const pedidoCliente=await Usuario.find({estado:'ClientePedido'},{nombreUsuario:1,apellidoUsuario:1,correo:1})

    console.log(pedidoCliente)
    res.json(pedidoCliente)

}

//LOS PEDIDOS QUE TENGO COMO VENDEDOR
export const listaVendedorPedidos=async(req,res)=>{
    const pedidoVendedor=await Usuario.find({estado:'VendedorPedido'},{nombreUsuario:1,apellidoUsuario:1,correo:1})

    console.log(pedidoVendedor)
    res.json(pedidoVendedor)

}

//DETALLE PEDIDO PARA EL CLIENTE
export const detallePedidoCliente=async(req,res)=>{
    const detalle=await Pedido.findOne({_id:req.params.pedidoId},{codigoQR:1,fechaDeEntrega:1,horaDeEntrega:1})
    res.json(detalle);
}

//DETALLE PEDIDO PARA EL VENDEDOR
export const detallePedidoVendedor=async(req,res)=>{
    const detalle=await Pedido.findOne({_id:req.params.pedidoId},{fechaDeEntrega:1,horaDeEntrega:1})
    res.json(detalle);
}