import Producto from "../models/producto";
import Categoria from "../models/categoria";
//import Reserva from "../models/reserva";
import cloudinary from "./../cloudinary_config";
import fs from 'fs-extra';

//CREAR PRODUCTOS
export const crearProducto=async(req,res)=>{
    
    const { nombreProducto,imgProducto,stock,precio,categoria,usuarioId,reservaId }=req.body;
    //Guardar producto
    const nuevoProducto=new Producto({
        nombreProducto,
        imgProducto,
        stock,
        precio,
        usuarioId,
        reservaId
    });

    if(req.body.categoria){
        const identificarCategoria= await Categoria.find({nombreCategoria: { $in: categoria } });
        nuevoProducto.categoria=identificarCategoria.map((categoriaId)=>categoriaId._id );
    } else {
        const categoria=await Categoria.findOne({ nombreCategoria: 'Abarrotes' });
        nuevoProducto.categoria=[categoria._id];
    }


    const productoAgregado = await nuevoProducto.save();

    res.status(201).json({mensaje:'Producto agregado correctamente',productoAgregado});
}

//LISTAR PRODUCTOS
export const listarProductos=async(req,res)=>{
    const productos=await Producto.find();
    res.json(productos);
}


//ACTUALIZAR PRODUCTO
export const actualizarProducto=async(req,res)=>{
    const productoActualizado=await Producto.findByIdAndUpdate(req.params.productoId,req.body,{
        new:true
    })

    res.status(200).json(productoActualizado)
}

//ELIMINAR PRODUCTO
export const eliminarProducto=async(req,res)=>{
    const {productoId}=req.params;
    await Producto.findByIdAndDelete(productoId);
    res.status(204).json();
}

//Actualizar
export const actualizarTodo=async(req,res)=>{
    const actualizarProductos=await Producto.updateMany({},{
        descripcion:'Descripcion del producto',
        stock:12
    })
    console.log(actualizarProductos)
    res.status(204).json(actualizarProductos);
}

//DETALLE PRODUCTO PARA EL CLIENTE
export const detalleProductoCliente=async(req,res)=>{
    const producto=await Producto.findOne({_id:req.params.productoId},{nombreProducto:1,imgProducto:1,categoria:1,descripcion:1})
    res.json(producto);
}

//DETALLE PRODUCTO PARA EL VENDEDOR
export const detalleProductoVendedor=async(req,res)=>{
    const producto=await Producto.findOne({_id:req.params.productoId},{nombreProducto:1,imgProducto:1,categoria:1,descripcion:1,stock:1})
    res.json(producto);
}

//RESERVAR PRODUCTO
export const reservarProducto=async(req,res)=>{

    const productoReservado=await Producto.findByIdAndUpdate(req.params.productoId,req.body,{
        new:true
    })

    res.status(201).json({mensaje:'El producto a sido reservado',productoReservado});
} 