
import Categoria from "../models/categoria";
import Puesto from "../models/puestos";
import Usuario from "../models/usuario";
import Rol from "../models/rol";

//CREAR PUESTO
export const crearPuesto=async(req,res,next)=>{
    
    try {
        const { dni,nroPuesto,estado,color,categoriaId,vendedorId }=req.body;
        //Guardar producto
        const nuevoPuesto=new Puesto({
            dni,
            nroPuesto,
            estado,
            color
        });
        //buscar categoria
        if(req.body.categoriaId){
            const identificarCategoria= await Categoria.find({nombreCategoria: { $in: categoriaId } });
            nuevoPuesto.categoriaId=identificarCategoria.map((categoriaId)=>categoriaId._id );
        } else {
            const categoria=await Categoria.findOne({ nombreCategoria: 'Abarrotes' });
            nuevoProducto.categoria=[categoria._id];
        }
        //Buscar vendedor
        if(req.body.vendedorId){
            const identificarVendedor= await Usuario.find({correo: { $in: vendedorId } });
            nuevoPuesto.vendedorId=identificarVendedor.map((vendedorId)=>vendedorId._id );
        } else {
            console.log('No es vendedor')
        }
        //guardar en la base de datos
        const puestoAgregado = await nuevoPuesto.save();
        //respuesta del servidor
        res.status(201).json({mensaje:'Nuevo puesto agregado',puestoAgregado});

    } catch (error) {
        next(error)
    }
}
//LISTAR PUESTO POR CATEGORIA
export const detalleVendedor=async(req,res,next)=>{
    const usuarioId=await Usuario.findOne({_id: req.params.puestoId});
    const usuario=usuarioId._id

    try {
        const usuarios=await Usuario.aggregate([
            {
                $match:{
                    _id:usuario
                }
            },
            {
                $lookup:{
                    from:'puestos',
                    localField:'_id',
                    foreignField:'vendedorId',
                    as:'Vendedores'
                }
            },
            {
                $unwind:'$Vendedores'
            },
            {
                $project:{
                     imgPuesto:'$imgPerfil',
                     nombre:'$nombreUsuario',
                     apellido:'$apellidoUsuario',
                     correo:'$correo',
                     edad:'$edad',
                     genero:'$genero',
                     nroPuesto:'$Vendedores.nroPuesto',
                     estado:'$Vendedores.estado',
                     color:'$Vendedores.color',
                     categoria:'$Vendedores.categoriaId'
                }
            },
        ])
        

        res.json(usuarios)

    } catch (error) {
        next(error)
    }
}
//LISTAR PUESTO POR CATEGORIA
export const listarPuestosPorCategorias=async(req,res,next)=>{
    const categoria=await Categoria.findOne({ _id: req.params.categoriaId });
    const categorias= categoria._id;
    try {
        const usuarios=await Usuario.aggregate([
            {
                $lookup:{
                    from:'puestos',
                    localField:'_id',
                    foreignField:'vendedorId',
                    as:'Vendedores'
                }
            },
            {
                $unwind:'$Vendedores'
            },
            {
                $project:{
                     imgPuesto:'$imgPerfil',
                     nombre:'$nombreUsuario',
                     nroPuesto:'$Vendedores.nroPuesto',
                     estado:'$Vendedores.estado',
                     color:'$Vendedores.color',
                     categoria:'$Vendedores.categoriaId'
                }
            },
            {
                $match:{
                    categoria:categorias
                }
            }
        ])
        
       console.log(usuarios.length);
        res.json(usuarios)

    } catch (error) {
        next(error)
    }
}
//LISTAR PUESTOS 
export const listarPuestos=async(req,res,next)=>{
    const rol=await Rol.findOne({ tipoDeRol: "Vendedor" });
    const roles=rol._id;
    try {
        const vendedores=await Usuario.find({rolId:roles},{imgPerfil:1,nombreUsuario:1})
        console.log(vendedores.length)
        res.status(201).json(vendedores);
    } catch (error) {
        next(error)
    }
}

