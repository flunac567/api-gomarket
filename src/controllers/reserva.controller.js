import Reserva from "../models/reserva";

//CREAR PRODUCTOS
export const crearReserva=async (req,res)=>{

    const { cantidad,precio ,pedidoId } =req.body
    
    const nuevaReserva=new Reserva({
          cantidad,
          precio,
          pedidoId
    });

    const reservaAgregada= await nuevaReserva.save();

    res.status(201).json({mensaje:'Reserva agregada',reservaAgregada})

}
//ELIMINAR RESERVA
export const eliminarReserva=async (req,res)=>{
    const {reservaId}=req.params
    await Reserva.findByIdAndDelete(reservaId);
    res.status(204).json({mensaje:'Reserva eliminada'});
} 
