import Usuario from "./../models/usuario";
import Rol from "./../models/rol";
import Categoria from "./../models/categoria";
import Producto from "./../models/producto";

//DETALLE DDE LOS DATOS DEL PRESIDENTE DE LA ASOCIACION
export const detallePresidente=async(req,res)=>{
    const presidente=await Usuario.findOne({correo:'pepeal@gmail.com'},{imgPerfil:1,nombreUsuario:1,apellidoUsuario:1,correo:1})
    res.json(presidente);
}
//DETALLE DDE LOS DATOS DEL VENDEDOR
export const detalleVendedor=async(req,res)=>{
    const vendedor=await Usuario.findOne({_id:req.params.usuarioId},{imgPerfil:1,nombreUsuario:1,apellidoUsuario:1,nroPuesto:1,correo:1})
    res.json(vendedor);
}

//DETALLE DDE LOS DATOS DEL CLIENTE
export const detalleCliente=async(req,res)=>{
    const cliente=await Usuario.findOne({_id:req.params.usuarioId},{imgPerfil:1,nombreUsuario:1,apellidoUsuario:1,correo:1})
    res.json(cliente);
}
//LISTAR VENDEDORES
export const listarVendedores=async(req,res)=>{

const rol=await Rol.findOne({ tipoDeRol: "Vendedor" });
const roles=rol._id;

const vendedores= await Rol.aggregate(
    [
     {
         $match: {
             _id: roles
         }  
     },
     {
        $lookup: {
            from:'usuarios',
            localField:'_id',
            foreignField:'rolId',
            as:'Vendedores'
        } 
     },
     {
        $unwind: '$Vendedores'
     },
    {
        $project: {
            Nombre:'$Vendedores.nombreUsuario',
            Apellido:'$Vendedores.nombreUsuario',
            Correo:'$Vendedores.correo',
            Categoria:'$Vendedores.categoriaId'
        }
    }
    ]
)
    res.json(vendedores)
}

//CONTAR VENDEDORES TOTALES
export const contarVendedores=async(req,res)=>{
   
    const rol=await Rol.findOne({ tipoDeRol: "Vendedor" });
    const roles=rol._id;
    
    const totalVendedores= await Rol.aggregate(
        [
         {
             $match: {
                 _id: roles
             }  
         },
         {
            $lookup: {
                from:'usuarios',
                localField:'_id',
                foreignField:'rolId',
                as:'Vendedores'
            } 
         },
         {
            $unwind: '$Vendedores'
         },
         {
            $count: 'Total de Vendedores'
         }
        ]
    )
        res.json(totalVendedores)
}

//MOSTRAR VENDEDORES POR CATEGORIA
export const vendedoresPorCategoria=async(req,res)=>{

    const categoria=await Categoria.findOne({ nombreCategoria: req.body.nombreCategoria });
    const categorias= categoria._id
    
    const listaDeVendedoresPorCategorias= await Categoria.aggregate(
        [
            {
                $match: {
                    _id: categorias
                } 
            },
            {
                $lookup: {
                    from:'usuarios',
                    localField:'_id',
                    foreignField:'categoriaId',
                    as:'Vendedores'
                } 
             },
             {
                $unwind: '$Vendedores'
             },
            {
                $project: {
                    Nombre:'$Vendedores.nombreUsuario',
                    Apellido:'$Vendedores.nombreUsuario',
                    Correo:'$Vendedores.correo',
                    Categoria:'$Vendedores.categoriaId'
                }
            }
        ]
        )

        console.log(listaDeVendedoresPorCategorias)
    
        res.json(listaDeVendedoresPorCategorias)
    }

//PRODUCTOS POR VENDEDOR
export const productosPorVendedor=async(req,res)=>{
    const usuario=await Usuario.findOne({ _id: req.body.usuarioId });
    const usuarios= usuario._id
    
    const listaDeProductosPorVendedor= await Usuario.aggregate(
        [
            {
                $match: {
                    _id: usuarios
                }
            },
            {
                $lookup: {
                    from:'productos',
                    localField:'_id',
                    foreignField:'usuarioId',
                    as:'Productos'
                } 
             },
             {
                $unwind: '$Productos'
             },
            {
                $project: {
                    Nombre:'$Productos.nombreProducto',
                    Descripcion:'$Productos.descripcion',
                    Precio:'$Productos.precio'
                }
            }
        ]
    )

    console.log(listaDeProductosPorVendedor)
    res.json(listaDeProductosPorVendedor)
}

//Actualizar
export const actualizarTodo=async(req,res)=>{
    const actualizarUsuario=await Usuario.updateMany({},{
        estado:'ClientePedido'
    })
    console.log(actualizarUsuario)
    res.status(204).json(actualizarUsuario);
}

//OBTENER DATOS
export const datosUsuario=async(req,res)=>{
    res.status(200).json({
         data:req.user
        
    });
}

