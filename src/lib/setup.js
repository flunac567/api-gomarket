import Rol from "../models/rol";
import Categoria from "../models/categoria";
import usuario from "../models/usuario";

import bcrypt from "bcryptjs";

//ROLES POR DEFAULT
export const crearRoles=async ()=>{
   try {

    const count=await Rol.estimatedDocumentCount()
    //Si no hay roles
    if(count > 0) return;
    //Crealos
    const value=await Promise.all([
        new Rol({ tipoDeRol: 'Cliente'}).save(),
        new Rol({ tipoDeRol: 'Vendedor'}).save(),
        new Rol({ tipoDeRol: 'Presidente'}).save(),
        new Rol({ tipoDeRol: 'Administrador'}).save()
    ]);
    //Mostrar roles
    console.log(value);
   } catch (error) {
     console.log(error);
   }
}
//CATEGORIAS POR DEFAULT
export const crearCategorias= async ()=>{
   try {
    const count=await Categoria.estimatedDocumentCount()
    if(count > 0) return;
    const listacategorias=await Promise.all([
      new Categoria({ nombreCategoria: 'Lacteos',imgCategoria:'https://res.cloudinary.com/pbpc/image/upload/v1606185923/GoMarket/c-lacteos_d2z1zz.png'}).save(),
      new Categoria({ nombreCategoria: 'Frutas',imgCategoria:'https://res.cloudinary.com/pbpc/image/upload/v1606185904/GoMarket/c-frutas_yjcowl.png'}).save(),
      new Categoria({ nombreCategoria: 'Verduras',imgCategoria:'https://res.cloudinary.com/pbpc/image/upload/v1606187003/GoMarket/c-verduras_is0n0s.jpg'}).save(),
      new Categoria({ nombreCategoria: 'Abarrotes',imgCategoria:'https://res.cloudinary.com/pbpc/image/upload/v1606186989/GoMarket/c-abarrotes_zhdgaw.jpg'}).save(),
      new Categoria({ nombreCategoria: 'Carnes',imgCategoria:'https://res.cloudinary.com/pbpc/image/upload/v1606185865/GoMarket/c-carnes_zoxiym.png'}).save(),
      new Categoria({ nombreCategoria: 'Pescados',imgCategoria:'https://res.cloudinary.com/pbpc/image/upload/v1606185984/GoMarket/c-pescados_d9nlb2.png'}).save(),
      new Categoria({ nombreCategoria: 'Higiene',imgCategoria:'https://res.cloudinary.com/pbpc/image/upload/v1606185949/GoMarket/c-limpieza_u2vf1g.png'}).save()
     ]);

     console.log(listacategorias);
   } catch (error) {
     console.log(error);
   }
}

//CREAR PRESIDENTE
export const crearPresidente= async()=>{

    //verificar si existe
  const presidente=await usuario.findOne({ correo: 'pepeal@gmail.com' });

  const roles= await Rol.find({tipoDeRol:{$in: ['Presidente']}});

  if(!presidente){
    await usuario.create({
      imgPerfil:'https://res.cloudinary.com/pbpc/image/upload/v1606876329/21766557_120731031960262_835918549670115081_n_c419pr.jpg',
      nombreUsuario:"Pepe Alnivar",
      apellidoUsuario:"Roldan Santiago",
      correo:"pepeal@gmail.com",
      contraseña:await bcrypt.hash("P@ssword123",12),
      rolId: roles.map((rol) => rol._id ),
      edad:"53"
    });
    console.log("se creo el usuario Presidente")
  }
}