import jwt from "jsonwebtoken";
import config from "../config";
import Usuario from "../models/usuario";
import Rol from "../models/rol";

//VERIFICACION DEL TOKEN
export const verificarToken = async(req,res, next)=>{
    try {
        let token = req.headers.authorization.split(' ')[1];

         if (!token) return res.status(403).json({ message: "No recibe ningun token" });

          const decodificador=jwt.verify(token,config.SECRET);
          console.log(decodificador);
          req.usuarioId=decodificador.id
          const usuario=await Usuario.findById(req.usuarioId ,{ contraseña: 0 });
          console.log(usuario);
          if(!usuario) return res.status(404).json({message:'No se encuentra el usuario'});
    
          next()

    } catch (error) {
        res.status(401).json({message:'No autorizado'});        
    }
}

export const verificarPass=async(req,res,next)=>{
    try {
		if (!req.headers.authorization) {
			return res.status(401).send('Unauhtorized Request');
		}
		let token = req.headers.authorization.split(' ')[1];
		if (token === 'null') {
			return res.status(401).send('No permitido');
		}

		const payload = await jwt.verify(token, config.SECRET);
		if (!payload) {
			return res.status(401).send('Unauhtorized Request');
		}
        req.userId = payload._id;
        const usuario=await Usuario.findById(req.userId ,{ contraseña: 0 });
        console.log(usuario);
		next();
	} catch(e) {
		//console.log(e)
		return res.status(401).send('Unauhtorized Request');
	}
}


export const verificarFacil=async(req,res,next)=>{
        let token = req.headers["x-access-token"];

        if (!token) return res.status(403).json({ message: "No recibe ningun token" });

        try {
            const verificar=jwt.verify(token,config.SECRET)
            req.user= verificar
            next()
        } catch (error) {
            res.status(400).json({error:'Token no valido'})
        }
}

//VALIDACION DE PERMISOS

//PERMISOS PARA EL CLIENTE
export const esCliente=async(req,res,next)=>{
    const usuario=await Usuario.findById(req.usuarioId);
    const roles = await Rol.find({_id: {$in: usuario.rolId}})

    for (let i = 0; i < roles.length; i++) {
        if(roles[i].tipoDeRol ==="Cliente") {
            next()
            return;
         }
     }
     return res.status(403).json("Solo los clientes pueden realizar esta acción");
    
}

//PERMISOS PARA EL VENDEDOR
export const esVendedor=async(req,res,next)=>{
    const usuario=await Usuario.findById(req.usuarioId);
    const roles = await Rol.find({_id: {$in: usuario.rolId}})

    for (let i = 0; i < roles.length; i++) {
       if(roles[i].tipoDeRol ==="Vendedor") {
           next()
           return;
        }
    }
    return res.status(403).json("Solo los vendedores pueden realizar esta acción");
}

//PERMISOS PARA EL PRESIDENTE
export const esPresidente = async(req,res,next)=>{
    const usuario = await Usuario.findById(req.usuarioId);
    const roles = await Rol.find({_id: {$in: usuario.rolId}})

    for (let i = 0; i < roles.length; i++) {
       if(roles[i].tipoDeRol ==="Presidente") {
           next()
           return;
        }
    }
    return res.status(403).json("Solo el presidente de la Asociación de Vendedores puede realizar esta acción");
}

