//import passport from "passport";
//import { ExtractJwt } from "passport-jwt";
var jwtStrategy=require('passport-jwt').Strategy,ExtractJwt=require('passport-jwt').ExtractJwt;
import config from "../config";
import Usuario from "../models/usuario";

let opts={
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: config.SECRET
}

module.exports=new jwtStrategy(opts,(jwt_payload,done)=>{
    Usuario.findById(jwt_payload.id,(err,usuario)=>{
        if(err) {
            return done(err,false);
        }
        if(usuario) {
            return(null,usuario);
        } 
        else{
             return done(null,false);
        }
    });
});


