import { Schema, model } from "mongoose";

const categoriaSchema=new Schema(
    {
        nombreCategoria:{ 
            type:String,
            required:true,
            unique:true    
        },
        imgCategoria:{ 
            type:String,
            required:true    
        },
        publicId: String
    },
    {
        timestamps:true,
        versionKey:false
    }
)

export default model("Categoria",categoriaSchema);