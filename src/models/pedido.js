import { Schema, model } from "mongoose";

const pedidoSchema=new Schema(
    {
        fechaDeEntrega:{
            type:Date,
            default: Date.now
        },
       estado:{
           type:String,
           enum: ['En Proceso', 'Finalizado'],
       },
       codigoQR:{
           type:String
       },
       codigoQR_Id: {
           type:String
       },
       clienteId:[
        {
            type:Schema.Types.ObjectId,
            ref:'Usuario'
        },
       ],
       vendedorId:[
        {
            type:Schema.Types.ObjectId,
            ref:'Puesto'
        },
       ],
       reservas:[{
           productoId:[{type:Schema.Types.ObjectId,ref:'Producto'}],
           cantidad:{type:Number,default:0},
           precioTotal:[{type:Number,default:0}]
        }]
    },
    {
     timestamps:true,
     versionKey:false
    }
)

export default model("Pedido",pedidoSchema);