import { Schema, model } from "mongoose";

const productoSchema=new Schema(
    {
        nombreProducto:{ type:String,required:true,unique:true},
        imgProducto:{ type:String,required:true },
        stock:{ type:Number,required:true,default:0 },
        precio:{ type:Number,required:true,default:0 },
        categoria:{ type:Schema.Types.ObjectId,ref:'Categoria' },
        usuarioId:{ type:Schema.Types.ObjectId,ref:'Usuario' },
        reservaId:{ type:Schema.Types.ObjectId,ref:'Reserva' },
    },
    {
        timestamps:true,
        versionKey:false
    }
)

export default model("Producto",productoSchema);