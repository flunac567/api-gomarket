import { Schema, model } from "mongoose";

const puestosSchema=new Schema(
    {
       dni:{
         type:String,
         unique:true
       },
       nroPuesto:{
           type:String,
           required:true
       },
       estado:{
           type:String,
           enum: ['Abierto', 'Cerrado'],
       },
       color:{
        type:String,
        enum: ['success', 'danger'],
       },
       categoriaId:[
          {
            type:Schema.Types.ObjectId,
            ref:'Categoria'
           }
        ],
       vendedorId:[
          {
            type:Schema.Types.ObjectId,
            ref:'Usuario'
          },
       ]
    },
    {
     timestamps:true,
     versionKey:false
    }
)

export default model("Puesto",puestosSchema);