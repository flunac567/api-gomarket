import { Schema, model } from "mongoose";

const reservaSchema=new Schema(
    {
       cantidad: {
           type:Number,
           default:0
       },
       precio:{
           type:Number,
           default:0
       },
       pedidoId:[
        {
            type:Schema.Types.ObjectId,
            ref:'Pedido'
        },
       ]
    },
    {
     timestamps:true,
     versionKey:false
    }
)

export default model("Reserva",reservaSchema);