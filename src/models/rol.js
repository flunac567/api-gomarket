import { Schema, model } from "mongoose";

export const ROLES = ["Cliente", "Vendedor", "Presidente","Administrador"];

const rolSchema=new Schema(
    {
        tipoDeRol:{
            type:String,
            unique:true
        }
    },
    {
        versionKey:false
    }
)

export default model("Rol",rolSchema);