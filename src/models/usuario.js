import { Schema, model } from "mongoose";
import bcrypt from "bcryptjs";

const usuarioSchema=new Schema(
    {
        imgPerfil:{
            type:String,
            required:true
        },
        nombreUsuario:{
            type:String,
            required:true
        },
        apellidoUsuario:{
            type:String,
            required:true
        },
        edad:{
            type:String,
            required:true
        },
        genero:{
            type:String,
            enum: ['Masculino', 'Femenino'],
        },
        correo:{
            type:String,
            required:true,
            match: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
            unique:true
        },
        contraseña:{
            type:String,
            required:true,
            match: /(?=.*[a-zA-Z])(?=.*[0-9]+).*/,
            minlength: 8
        },
        imgPerfilId:{
            type:String
        },
        rolId: [
            {
                type:Schema.Types.ObjectId,
                ref:'Rol',
            },
        ],
    },
    {
        timestamps:true,
        versionKey:false
    }
);

//Encriptar Contraseña
usuarioSchema.statics.encryptPassword = async(contraseña)=>{
    const salt=await bcrypt.genSalt(10);
    return await bcrypt.hash(contraseña,salt);
}
//Comparar
usuarioSchema.statics.comparePassword = async (contraseña, nuevaContraseña) => {
    return await bcrypt.compare(contraseña, nuevaContraseña)
  }

export default model("Usuario",usuarioSchema);