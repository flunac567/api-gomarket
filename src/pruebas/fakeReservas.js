//const faker=require('faker');
const { database } = require('faker/lib/locales/en');
const faker=require('faker/locale/es');
const fs=require('fs');

function generateReservas() {
    let reservas=[];

    for (let id = 1; id <= 100; id++) {
        const _id=faker.random.uuid();
        const codigo= faker.random.word();
        const cantidad=faker.random.number(100);
        const precio= faker.random.number(30);
        const horaDeEntrega=faker.date.recent();
        
        reservas.push({
            id: id,
            _id: _id,
           codigo:codigo,
           cantidad:cantidad,
           precio:precio,
           horaDeEntrega:horaDeEntrega
        });

    }

    return {data: reservas}
}

const generarData= generateReservas();

console.log(generarData);

fs.writeFileSync('fakeReserva.json',JSON.stringify(generarData),null,"\t");

