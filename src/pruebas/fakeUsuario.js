//const faker=require('faker');
import exportFromJSON from 'export-from-json'
const { database } = require('faker/lib/locales/en');
const faker=require('faker/locale/es');
const fs=require('fs');


function generateUsuarios() {
    let users=[];

    for (let id = 1; id <= 1000; id++) {
        const _id=faker.random.uuid();
        const img= faker.random.image();
        const nombre=faker.name.firstName();
        const apellido=faker.name.lastName();
        
        users.push({
            id: id,
            _id: _id,
            firstName:nombre,
            lastName:apellido,
            image:img
        });

    }

    return {data: users}
}

const generarData= generateUsuarios();

console.log(generarData);

//fs.writeFileSync('data.json',JSON.stringify(generarData),null,"\t");

