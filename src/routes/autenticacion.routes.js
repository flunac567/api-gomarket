import { Router } from "express";
const router= Router();
//import passport from "passport";
import multer from '../middlewares/multers'

import * as autenticacionCtrl from "../controllers/autenticacion.controller";

router.post('/registrarse',multer.single('imgPerfil'),autenticacionCtrl.registro);
router.post('/loguearse',autenticacionCtrl.login);

/*
router.get('/estalogueado', passport.authenticate('jwt',{session:false}),(req,res)=>{
    return res.json({message:'Hola estas dentro !'})
});
*/

export default router;