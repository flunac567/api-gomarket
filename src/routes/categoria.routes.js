import { Router } from "express";
const router= Router();
import multer from '../middlewares/multers'

//Controladores
import * as categoriaCtrl from "../controllers/categoria.controller";
//Middlewares
import { autenticacionJWT} from "../middlewares";

//En single se pone el nombre que tiene el campo en la base de datos (en este caso imgCategoria)
router.post('/',[autenticacionJWT.verificarToken,autenticacionJWT.esPresidente],multer.single('imgCategoria'),categoriaCtrl.crearCategoria);
router.get('/',[autenticacionJWT.verificarToken,autenticacionJWT.esPresidente],categoriaCtrl.obtenerCategorias);
router.get('/categoriascliente/',categoriaCtrl.clienteCategorias);
router.get('/:categoriaId',categoriaCtrl.buscarCategoria);
router.put('/:categoriaId',[autenticacionJWT.verificarToken,autenticacionJWT.esPresidente],categoriaCtrl.actualizarCategoria);
router.get('/eliminar/:categoriaId',[autenticacionJWT.verificarToken,autenticacionJWT.esPresidente],categoriaCtrl.eliminarCategoriaCloud);

export default router;