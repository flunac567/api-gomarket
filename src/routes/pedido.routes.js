import { Router } from "express";
const router= Router();
//Controladores
import * as pedidoCtrl from "../controllers/pedido.controller";
//Middlewares
import { autenticacionJWT} from "../middlewares";

router.post('/:vendedorId',pedidoCtrl.crearPedido);
router.post('/pedidoProducto/:productoId',pedidoCtrl.reservarProductos);


router.get('/',pedidoCtrl.listarPedidos);
router.get('/clientes',pedidoCtrl.listaClientesPedidos);
router.get('/vendedor',pedidoCtrl.listaVendedorPedidos);
router.get('/detalleCliente/:pedidoId',pedidoCtrl.detallePedidoCliente);
router.get('/detalleVendedor/:pedidoId',pedidoCtrl.detallePedidoVendedor);
//router.post('/',pedidoCtrl.pruebaPedidos);
router.get('/cliente/:pedidoId',pedidoCtrl.clientePedido);
/*router.get('/:PedidoId',pedidoCtrl.buscarPedido);*/
router.put('/:pedidoId',pedidoCtrl.actualizarPedido);
router.get('/eliminar/:pedidoId',[autenticacionJWT.verificarToken,autenticacionJWT.esCliente],pedidoCtrl.eliminarPedidoCloud);
 
export default router;