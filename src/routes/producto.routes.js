import { Router } from "express";
const router= Router();
//Controladores
import * as productoCtrl from "../controllers/producto.controller";
//Middlewares
import { autenticacionJWT} from "../middlewares";

router.post('/',[autenticacionJWT.verificarToken,autenticacionJWT.esVendedor],productoCtrl.crearProducto);
router.get('/',productoCtrl.listarProductos);
router.get('/cliente/:productoId',productoCtrl.detalleProductoCliente);
router.get('/vendedor/:productoId',productoCtrl.detalleProductoVendedor);
router.put('/',productoCtrl.actualizarTodo);
router.put('/:productoId',[autenticacionJWT.verificarToken,autenticacionJWT.esVendedor],productoCtrl.actualizarProducto);
router.put('/reservar/:productoId',[autenticacionJWT.verificarToken,autenticacionJWT.esCliente],productoCtrl.reservarProducto);
router.delete('/:productoId',[autenticacionJWT.verificarToken,autenticacionJWT.esVendedor],productoCtrl.eliminarProducto);
//router.put('/reservar/:productoId',[autenticacionJWT.verificarToken,autenticacionJWT.esCliente],productoCtrl.reservarProducto);

export default router;