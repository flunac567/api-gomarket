import { Router } from "express";
const router= Router();

//Controladores
import * as puestoCtrl from "../controllers/puesto.controller";
//Middlewares
import { autenticacionJWT} from "../middlewares";

//En single se pone el nombre que tiene el campo en la base de datos (en este caso imgCategoria)
router.post('/',puestoCtrl.crearPuesto);
router.get('/:categoriaId',puestoCtrl.listarPuestosPorCategorias);
router.get('/',puestoCtrl.listarPuestos);
router.get('/detalle/:puestoId',puestoCtrl.detalleVendedor);
export default router;