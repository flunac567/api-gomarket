import { Router } from "express";
const router= Router();
//Controladores
import * as reservaCtrl from "../controllers/reserva.controller";
//Middlewares
import { autenticacionJWT} from "../middlewares";

router.post('/',reservaCtrl.crearReserva);
router.delete('/eliminar/:reservaId',reservaCtrl.eliminarReserva);
 
export default router;