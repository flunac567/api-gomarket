import { Router } from "express";
const router= Router();

import * as usuarioCtrl from "./../controllers/usuario.controller";
//Middlewares
import { autenticacionJWT} from "../middlewares";

router.get('/',usuarioCtrl.listarVendedores);
router.get('/contarVendedores',[autenticacionJWT.verificarToken,autenticacionJWT.esPresidente],usuarioCtrl.contarVendedores);
router.get('/vendedores',[autenticacionJWT.verificarToken,autenticacionJWT.esCliente],usuarioCtrl.vendedoresPorCategoria);
router.get('/productos',[autenticacionJWT.verificarToken,autenticacionJWT.esCliente],usuarioCtrl.productosPorVendedor);
router.get('/datos',autenticacionJWT.verificarToken,usuarioCtrl.datosUsuario);
router.put('/',usuarioCtrl.actualizarTodo);
router.get('/vendedor/:usuarioId',usuarioCtrl.detalleVendedor);
router.get('/cliente/:usuarioId',usuarioCtrl.detalleCliente);
router.get('/presidente',usuarioCtrl.detallePresidente);

export default router;